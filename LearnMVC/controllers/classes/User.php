<?php
require_once __DIR__. '/../init.php';
/**
 * 
 */
class User{ 

   function login($user, $db){
  
      if ( empty($user['email']) OR empty($user['password']) ){
      	return 'Invalid_Field';
      }

      $sql="SELECT * FROM `user` WHERE `email`= ?"; // bounded parameter or parameterised query
      $statement = $db->prepare($sql);

   if (is_object($statement)) {
   	  $statement->bindParam(1, $user['email'], PDO::PARAM_STR);
   	  $statement->execute();


   	  if($row=$statement->fetch(PDO::FETCH_OBJ)){
         
          if (password_verify( $user['password'], $row->password)) {
          	  $_SESSION['logged_in']=[
                 'id' => $row->id,
                 'id' => $row->name

          	  ];
            return 'success';
          }
          
   	   }
    }
    return 'error';
   }
// -----------------------------------------------------login-------------------------------------------
  

    function signup($user,$db){

      if ( empty($user['username']) OR empty($user['email_id']) OR empty($user['password']) OR empty($user['password_again'])) {

        return 'Invalid_Field';
      }
      else if ($user['password'] !== $user['password_again']) {
        return 'mismatch_password';
      }
      else if ($this->emailExist($user['email_id'],$db)) {
        return 'email_exists';
      }
      else {
       

        $sql="INSERT INTO `user`(`name`,`email`,`password` , `salt` ) VALUES(?,?,?,?)";
         $statement = $db->prepare($sql); 
   
         if (is_object($statement)) {
          $hash = password_hash($user['password'], PASSWORD_DEFAULT);
           $statement->bindParam(1, $user['username'], PDO::PARAM_STR);
           $statement->bindParam(2, $user['email_id'], PDO::PARAM_STR);
           $statement->bindParam(3, $hash, PDO::PARAM_STR);
           $statement->bindParam(4, $user['salt'], PDO::PARAM_STR);
//           $statement->bindParam(4, generateCode(), PDO::PARAM_STR);

           $statement->execute();

           if ($statement->rowCount()) {
             return 'success';
           }
         }
      }

      return 'error';
    
   }

   // -----------------------------------------------------/login----------------------------------------



 
    public function mailResetPasswordLink($user,$db){
    if (empty($user['email'])) {
      return "Invalid_Field";
    }else if (!$this->emailExist($user['email'],$db)) {
      return "email_exists";
    }
   $em=$user['email'];
     $sql="SELECT * FROM `user` WHERE `email`= '$em' "; // bounded parameter or parameterised query
      $statement = $db->prepare($sql);
   $statement->execute();
      
      
      if ($row = $statement->fetch()) {
        $status = $this->sendMail($row);
        if ($status) {
          return "success";
        }else{
          return "error";
        }
      }else{
        return "mismatch_password";
      }

   
   }


// // ----------------------------------email exists---------------------------------------- 
  
   private function emailExist($email,$db){
        $sql="SELECT * FROM `user` WHERE `email`= ?"; // bounded parameter or parameterised query
      $statement = $db->prepare($sql);

       if (is_object($statement)) {
      $statement->bindParam(1, $email, PDO::PARAM_STR);
      $statement->execute();


      if($row=$statement->fetch(PDO::FETCH_OBJ)){
          
          return true;
          
       }
    }
     return false;
   }
  // ----------------------------------/email exists---------------------------------------- 


    // ----------------------------------send email---------------------------------------- 

     public function sendMail($user){
  // Create the Transport
$transport = (new Swift_SmtpTransport('smtp.gmail.com', 587, 'tls'))
  // ->setUsername('826903d801ddc1')
  // ->setPassword('e257997dcd5a5a')

->setUsername('sty.racer@gmail.com')
  ->setPassword('9807794697')
;

// Create the Mailer using your created Transport
$mailer = new Swift_Mailer($transport);

// Create a message
$message = (new Swift_Message('Password recovery request'))
  ->setFrom(['noreply@team.com' => 'team-wri'])
  ->setTo([$user['email']])
  ->setBody(passwordEmailRecoverMessageBody($user), 'text/html')
  ;

// Send the message
$result = $mailer->send($message);


if ($result) {
  return true;
}else{
  return false;
}

 }


 
// ----------------------------------send email---------------------------------------- 
 public function changePassword($user,$db){



  
  // echo  $new_password = $_POST['new_password'];
  // echo  $confirm_password = $_POST['cfpassword'];


    if (empty($user['id_user'])) {
      return "Invalid_Field";
    }else if ($user['new_password'] !== $user['cfpassword']) {
      return "mismatch_password";
    }
    
   
     $user_id = $user['id_user'];
     $hash = password_hash($user['new_password'], PASSWORD_DEFAULT);
    // bounded parameter or parameterised query

     $sql = ("UPDATE `user` SET `password`= '$hash' WHERE `id` = '$user_id' ") ;

      $statement = $db->prepare($sql);
   $statement->execute();
      
      
     

       if($statement){
          return "success";
       }
       else{
          return "error";
        }
  
 }

}

// ----------------------------------/End of class---------------------------------------- 

 


$user = new User();




